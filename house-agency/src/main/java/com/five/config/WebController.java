package com.five.config;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {

    //主页
    @RequestMapping({"/admin", "/admin/index","/admin/index.html"})
    @PreAuthorize("hasPermission('/admin',null)")
    public String index(){
        return "index";
    }

    //首页
    @RequestMapping("/admin/main")
    @PreAuthorize("hasPermission('/admin/main',null)")
    public String main(){
        return "main";
    }

    //父菜单页
    @RequestMapping("/admin/menu/toMenuManager")
    @PreAuthorize("hasPermission('/admin/menu/toMenuManager',null)")
    public String menu(){
        return "menu/menuManager";
    }

    //菜单页--左
    @RequestMapping("/admin/menu/toMenuLeftManager")
    @PreAuthorize("hasPermission('/admin/menu/toMenuManager',null)")
    public String menu_left(){
        return "menu/menuLeft";
    }

    //菜单页--右
    @RequestMapping("/admin/menu/toMenuRightManager")
    @PreAuthorize("hasPermission('/admin/menu/toMenuManager',null)")
    public String menu_right(){
        return "menu/menuRight";
    }

    //父权限页
    @RequestMapping("/admin/permission/toPermissionManager")
    @PreAuthorize("hasPermission('/admin/permission/toPermissionManager',null)")
    public String permission(){
        return "permission/permissionManager";
    }

    //权限页--左
    @RequestMapping("/admin/permission/toPermissionLeftManager")
    @PreAuthorize("hasPermission('/admin/permission/toPermissionManager',null)")
    public String permission_left(){
        return "permission/permissionLeft";
    }

    //权限页--右
    @RequestMapping("/admin/permission/toPermissionRightManager")
    @PreAuthorize("hasPermission('/admin/permission/toPermissionManager',null)")
    public String permission_right(){
        return "permission/permissionRight";
    }

    //角色页
    @RequestMapping("/admin/role/toRoleManager")
    @PreAuthorize("hasPermission('/admin/role/toRoleManager',null)")
    public String role(){
        return "role/roleManager";
    }

    //员工页
    @RequestMapping("/admin/user/staffList")
    @PreAuthorize("hasPermission('/admin/user/staffList',null)")
    public String staff(){
        return "user/staffList";
    }

    //顾客页
    @RequestMapping("/admin/user/customerList")
    @PreAuthorize("hasPermission('/admin/user/customerList',null)")
    public String customer(){
        return "user/customerList";
    }

    //修改密码页
    @RequestMapping("/admin/user/changePwd")
    @PreAuthorize("hasPermission('/admin',null)")
    public String password(){
        return "user/changePwd";
    }

    //用户个人信息页
    @RequestMapping("/admin/user/userInfo")
    @PreAuthorize("hasPermission('/admin',null)")
    public String userInfo(){
        return "user/userInfo";
    }

    //城市页
    @RequestMapping("/admin/city/toCityManager")
    @PreAuthorize("hasPermission('/admin/city/toCityManager',null)")
    public String city(){
        return "configure/city";
    }

    //出售出租类别页
    @RequestMapping("/admin/sale/toSale")
    @PreAuthorize("hasPermission('/admin/sale/toSale',null)")
    public String sale(){
        return "configure/sale";
    }

    //装修风格页
    @RequestMapping("/admin/decoration/toDecoration")
    @PreAuthorize("hasPermission('/admin/decoration/toDecoration',null)")
    public String decoration(){
        return "configure/decoration";
    }

    //新房页
    @RequestMapping("/admin/newHouse/toNewHouseManager")
    @PreAuthorize("hasPermission('/admin/newHouse/toNewHouseManager',null)")
    public String newHouse(){
        return "house/newHouseList";
    }

    //二手房页
    @RequestMapping("/admin/newHouse/toSecondHouseManager")
    @PreAuthorize("hasPermission('/admin/newHouse/toSecondHouseManager',null)")
    public String secondHouse(){
        return "house/secondHouseList";
    }

    //租房页
    @RequestMapping("/admin/newHouse/toRentManager")
    @PreAuthorize("hasPermission('/admin/newHouse/toRentManager',null)")
    public String rentHouse(){
        return "house/rentHouseList";
    }

    //出售委托页
    @RequestMapping("/admin/entrust/toSellEntrust")
    @PreAuthorize("hasPermission('/admin/entrust/toSellEntrust',null)")
    public String sellEntrust(){
        return "entrust/sellEntrust";
    }

    //出租委托页
    @RequestMapping("/admin/entrust/toLeaseEntrust")
    @PreAuthorize("hasPermission('/admin/entrust/toLeaseEntrust',null)")
    public String leaseEntrust(){
        return "entrust/leaseEntrust";
    }

    //新闻页
    @RequestMapping("/admin/news/toNews")
    @PreAuthorize("hasPermission('/admin/news/toNews',null)")
    public String news(){
        return "news/news";
    }

    //订单页
    @RequestMapping("/admin/order/toOrder")
    @PreAuthorize("hasPermission('/admin/order/toOrder',null)")
    public String order(){
        return "order/orderList";
    }

    //城市员工年度销售额
    @RequestMapping("/admin/analysis/toCityStaffAnalysis")
    @PreAuthorize("hasPermission('/admin/analysis/toCityStaffAnalysis',null)")
    public String cityStaffAnalysis(){
        return "analysis/cityStaffAnalysis";
    }

    //城市年度销售额
    @RequestMapping("/admin/analysis/toCityAnalysis")
    @PreAuthorize("hasPermission('/admin/analysis/toCityAnalysis',null)")
    public String cityAnalysis(){
        return "analysis/cityAnalysis";
    }

    //员工每月销售额
    @RequestMapping("/admin/analysis/toStaffAnalysis")
    @PreAuthorize("hasPermission('/admin/analysis/toStaffAnalysis',null)")
    public String staffAnalysis(){
        return "analysis/staffAnalysis";
    }

    //其他信息
    @RequestMapping("/admin/analysis/toOtherAnalysis")
    @PreAuthorize("hasPermission('/admin/analysis/toOtherAnalysis',null)")
    public String otherAnalysis(){
        return "analysis/otherAnalysis";
    }
}
