package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.Sale;
import com.five.service.SaleService;
import com.five.vo.SaleVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/sale")
public class SaleController {

    @Autowired
    private SaleService saleService;

    //查询类别信息
    @RequestMapping("/allSale")
    @PreAuthorize("hasPermission('/admin/sale/toSale',null)")
    public DataGridView listSale(SaleVo saleVo){
        Page<Object> page = PageHelper.startPage(saleVo.getPage(), saleVo.getLimit());
        List<Sale> data = saleService.queryAllSale(saleVo);
        return new DataGridView(page.getTotal(), data);
    }

    //可用类别
    @RequestMapping("/getSale1")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView getSale1(SaleVo saleVo){
        saleVo.setAvailable(1);
        List<Sale> data = saleService.querySaleByUse(saleVo);
        return new DataGridView(data);
    }

    //查询导航栏
    @RequestMapping("/getSale2")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView getSale2(SaleVo saleVo){
        List<Sale> data = saleService.queryAllSale(saleVo);
        return new DataGridView(data);
    }

    //添加类别
    @RequestMapping("/addSale")
    @PreAuthorize("hasPermission('/admin/sale/addSale','sale:add')")
    public DataGridView addSale(SaleVo saleVo){
        return saleService.addSale(saleVo);
    }

    //修改类别
    @RequestMapping("/updateSale")
    @PreAuthorize("hasPermission('/admin/sale/updateSale','sale:update')")
    public DataGridView updateSale(SaleVo saleVo){
        return saleService.updateSale(saleVo);
    }

    //删除类别信息
    @RequestMapping("/deleteSale")
    @PreAuthorize("hasPermission('/admin/sale/deleteSale','sale:delete')")
    public DataGridView deleteSale(SaleVo saleVo){
        return saleService.deleteSale(saleVo);
    }
}
