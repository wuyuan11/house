package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.Order;
import com.five.service.OrderService;
import com.five.vo.OrderVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //查询所有订单
    @RequestMapping("/allOrder")
    @PreAuthorize("hasPermission('/admin/order/toOrder',null)")
    public DataGridView listOrder(OrderVo orderVo){
        Page<Object> page = PageHelper.startPage(orderVo.getPage(), orderVo.getLimit());
        List<Order> data = orderService.queryAllOrder(orderVo);
        return new DataGridView(page.getTotal(), data);
    }

    //取消订单
    @RequestMapping("/cancelOrder")
    @PreAuthorize("hasPermission('/admin/order/cancelOrder','order:cancel')")
    public DataGridView cancelOrder(OrderVo orderVo){
        try {
            orderService.cancelOrder(orderVo);
            return DataGridView.CANCEL_SUCCESS;
        }catch (Exception e){
            return DataGridView.CANCEL_ERROR;
        }
    }

}
