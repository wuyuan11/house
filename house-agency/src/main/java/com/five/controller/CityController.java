package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.City;
import com.five.service.CityService;
import com.five.vo.CityVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/city")
public class CityController {

    @Autowired
    private CityService cityService;

    //查询城市信息
    @RequestMapping("/allCity")
    @PreAuthorize("hasPermission('/admin/city/toCityManager',null)")
    public DataGridView listCity(CityVo cityVo){
        Page<Object> page = PageHelper.startPage(cityVo.getPage(), cityVo.getLimit());
        List<City> data = cityService.queryAllCity(cityVo);
        return new DataGridView(page.getTotal(), data);
    }

    //可用城市
    @RequestMapping("/getCity1")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView getCity1(CityVo cityVo){
        cityVo.setAvailable(1);
        List<City> data = cityService.queryCityByUse(cityVo);
        return new DataGridView(data);
    }

    //查询导航栏
    @RequestMapping("/getCity2")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView getCity2(CityVo cityVo){
        List<City> data = cityService.queryCityByUse(cityVo);
        return new DataGridView(data);
    }

    //添加城市
    @RequestMapping("/addCity")
    @PreAuthorize("hasPermission('/admin/city/addCity','city:add')")
    public DataGridView addCity(CityVo cityVo){
        return cityService.addCity(cityVo);
    }

    //修改城市
    @RequestMapping("/updateCity")
    @PreAuthorize("hasPermission('/admin/city/updateCity','city:update')")
    public DataGridView updateCity(CityVo cityVo){
        return cityService.updateCity(cityVo);
    }

    //删除(恢复)城市信息
    @RequestMapping("/deleteCity")
    @PreAuthorize("hasPermission('/admin/city/deleteCity','city:delete')")
    public DataGridView deleteCity(CityVo cityVo){
        return cityService.deleteCity(cityVo);
    }
}
