package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.Order;
import com.five.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/main")
public class MainController {
    @Autowired
    OrderService orderService;

    //销售额统计
    @RequestMapping("/loadOrderYear")
    @PreAuthorize("hasPermission('/admin/main','main:main')")
    public List<Double> loadOrderYear(Integer year){
        List<Double> entities = orderService.loadOrderYear(year);
        for (int i = 0; i < entities.size(); i++) {
            if (null==entities.get(i)) {
                entities.set(i, 0.0);
            }
        }
        return entities;
    }

    //订单总数
    @RequestMapping("/countOrder")
    @PreAuthorize("hasPermission('/admin/main','main:main')")
    public DataGridView countOrder(Order order){
        return orderService.countOrder(order);
    }

    //未付款订单总数
    @RequestMapping("/countNoPayOrder")
    @PreAuthorize("hasPermission('/admin/main','main:main')")
    public DataGridView countNoPayOrder(Order order){
        order.setStatus(1);
        return orderService.countOrder(order);
    }

    //已付款订单总数
    @RequestMapping("/countPayOrder")
    @PreAuthorize("hasPermission('/admin/main','main:main')")
    public DataGridView countPayOrder(Order order){
        order.setStatus(2);
        return orderService.countOrder(order);
    }
}
