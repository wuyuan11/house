package com.five.mapper;

import com.five.config.security.User;
import com.five.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    //根据用户名查询
    User loadUserByUsername(String username);
    //查询用户数据
    List<SysUser> queryAllUser(SysUser sysUser);
    //根据条件查询
    SysUser queryUserByAll(SysUser sysUser);
    //添加用户
    void addUser(SysUser sysUser);
    //修改用户
    void updateUser(SysUser sysUser);
    //保存用户和角色的关系
    void addUserRole(@Param("uid")String uid, @Param("rid")Integer rid);
    //统计用户总数
    Long countUser(@Param("available")Integer available, @Param("type")Integer type);
    //查询可用员工
    List<SysUser> queryStaffByUse();
}
