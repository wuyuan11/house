package com.five.mapper;

import com.five.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleMapper {
    //查询角色
    List<SysRole> queryAllRole(SysRole sysRole);
    //添加角色
    int addRole(SysRole sysRole);
    //修改角色
    int updateRole(SysRole sysRole);
    // 删除角色表的数据
    void deleteRole(Integer id);
    // 根据角色id删除sys_role_permission里面的数据
    void deleteRolePermissionByRid(@Param("rid")Integer id);
    // 根据角色id删除sys_role_user里面的数据
    void deleteRoleUserByRid(@Param("rid")Integer id);
    //保存角色和菜单之间的关系
    void addRolePermission(@Param("rid")Integer rid, @Param("pid")Integer pid);
    //根据用户ID查询已拥有的角色
    List<SysRole> queryRoleByUid(@Param("available")Integer available, @Param("uid")String uid);
    //根据用户id删除sys_role_user里面的数据
    void deleteRoleUserByUid(@Param("uid") String uid);
    //根据角色名查询
    SysRole queryRoleByName(String name);
}
