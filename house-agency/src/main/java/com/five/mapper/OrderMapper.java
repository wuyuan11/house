package com.five.mapper;

import com.five.domain.BaseEntity;
import com.five.domain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {

    //查询订单
    List<Order> queryAllOrder(Order order);

    void alter();

    //查看订单
    Order lookOrder(Order order);

    //取消订单
    void updateOrder(Order order);

    //购买订单
    void addOrder(Order order);

    //条件查询
    List<Order> queryAllOrderByOther(Order order);

    //统计年度销售额
    List<Double> loadOrderYear(@Param("year")Integer year, @Param("city")Integer city,@Param("staff")String staff,@Param("agent")String agent);

    //获取员工年度销售额
    List<BaseEntity> LoadStaffCity(@Param("year")Integer year, @Param("city") Integer city);

    //统计用户总数
    Long countOrder(@Param("uid")String uid, @Param("status")Integer status);
}
