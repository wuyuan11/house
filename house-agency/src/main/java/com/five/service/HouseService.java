package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.House;
import com.five.vo.HouseVo;

import java.util.List;

public interface HouseService {

    //后端页面====================================================


    //查询房屋
    List<House> queryAllHouse(HouseVo houseVo);

    //添加房屋
    DataGridView addHouse(HouseVo houseVo);

    //修改房屋
    DataGridView updateHouse(HouseVo houseVo);

    //取消房屋
    DataGridView cancelHouse(HouseVo houseVo);

    //房屋数量
    DataGridView queryHouseNumber(House house);

    //查询房屋参数(App与后端共用)
    House queryHouseById(HouseVo houseVo);


    //小程序====================================================


    //查询房屋(App)
    List<House> queryHouse(HouseVo houseVo);

    //最新房屋(App)
    List<House> queryHouseByNew(HouseVo houseVo);

}
