import {request} from "../../request/index.js";

Page({

  data: {
  },

  onShow: function () {
    if(wx.getStorageSync("cookieKey")==null||wx.getStorageSync("cookieKey")==""){
      wx.reLaunch({
        url: '../login/index',
      });
      return;
    }
    this.data.inputnum=1;
    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    let options = currentPage.options;
    const {id}=options;
    this.loadOrder(id);
  },
  async loadOrder(id){
    //创建header
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/lookOrder", method: "POST" ,data:{id:id},header: header });
    const {code} = res.data;
    if(code===200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
      }
      this.setData({
        house:res.data.data
      })
    }else{
      wx.reLaunch({
        url: '../login/index',
      })
    }
  },
  cancel(e){
    var that=this;
    wx.showModal({
      title: '取消订单',
      content: '确认要取消这个订单吗？',
      success: function (res) {
        if (res.confirm) {  
          that.cancel2(e);
        } else {   
          return;
        }
      }
    })
  },
  async cancel2(e){
    const {id} = e.currentTarget.dataset;
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/cancelOrder", method: "POST" ,data:{id:id} ,header: header });
    const {code,msg} = res.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(code==200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
      }
      setTimeout(() => {
        // 返回上一个页面
        this.onShow();
      }, 1000);
    }else if(code==500){
      wx.reLaunch({
        url: '../login/index',
      })
    }
  },
  pay(e){
    if(this.data.inputnum==null||this.data.inputnum==""){
      wx.showToast({
        title: '租用月数不能为空',
        icon: 'none',
        mask: true
      });
      return;
    }
    var that=this;
    wx.showModal({
      title: '确认支付',
      content: '确认要支付这个订单吗？',
      success: function (res) {
        if (res.confirm) { 
          that.pay2(e);
        } else {   
          return;
        }
      }
    })
  },
  async pay2(e){
    const {id} = e.currentTarget.dataset;
    const inputnum = this.data.inputnum;
    let header;
    header = {
      'content-type': 'application/x-www-form-urlencoded',
      'cookie':wx.getStorageSync("cookieKey")//读取cookie
    };
    const res = await request({ url: "/user/order/payOrder", method: "POST" ,data:{id:id,inputnum:inputnum} ,header: header });
    const {code,msg} = res.data;
    wx.showToast({
      title: msg,
      icon: 'none',
      mask: true
    });
    if(code==200){
      if (res && res.header && res.header['Set-Cookie']) {
        wx.setStorageSync('cookieKey', res.header['Set-Cookie']);   //保存Cookie到Storage
      }
      setTimeout(() => {
        this.onShow();
      }, 1000);
    }
    else if(code==500){
      wx.redirectTo({
        url: '../login/index',
      })
    }
  },
  inputNum(e){
    this.data.inputnum = e.detail.value;
  },
  //拨打电话
  Tel: function (e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone // 仅为示例，并非真实的电话号码
    })
  },
    
})